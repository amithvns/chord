package chord.implementation;

public class ChordException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChordException() {
		super();
	}

	public ChordException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChordException(String message) {
		super(message);
	}

	public ChordException(Throwable cause) {
		super(cause);
	}

}
